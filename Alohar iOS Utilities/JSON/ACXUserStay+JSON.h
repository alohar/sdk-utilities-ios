//
//  ACXUserStay+JSON.h
//  Alohar Mobile Sample
//
//  Created by Steven Byrd on 9/22/15.
//
//

@import Foundation;

#import <Alohar/ACXUserStay.h>


extern NSString *const ACXUserStayIdentifierKey;
extern NSString *const ACXUserStayCentroidCoordinateKey;
extern NSString *const ACXUserStayStartDateKey;
extern NSString *const ACXUserStayEndDateKey;
extern NSString *const ACXUserStaySelectedPlaceKey;
extern NSString *const ACXUserStayTopCandidatePlacesKey;


#pragma mark -
@interface ACXUserStay (JSON)

- (NSDictionary *)JSONDictionary;

@end
