//
//  ACXUserStay+JSON.m
//  Alohar Mobile Sample
//
//  Created by Steven Byrd on 9/22/15.
//
//

#import "ACXJsonUtils.h"
#import "ACXPlace+JSON.h"
#import "ACXUserStay+JSON.h"


NSString *const ACXUserStayIdentifierKey            = @"identifier";
NSString *const ACXUserStayCentroidCoordinateKey    = @"centroidCoordinate";
NSString *const ACXUserStayStartDateKey             = @"startDate";
NSString *const ACXUserStayEndDateKey               = @"endDate";
NSString *const ACXUserStaySelectedPlaceKey         = @"selectedPlace";
NSString *const ACXUserStayTopCandidatePlacesKey    = @"topCandidatePlaces";


#pragma mark -
@implementation ACXUserStay (JSON)

- (NSDictionary *)JSONDictionary {
    return @{
        ACXUserStayIdentifierKey          : (self.identifier ?: [NSNull null]),
        ACXUserStayCentroidCoordinateKey  : [ACXJsonUtils JSONDictionaryForCoordinate:self.centroidCoordinate],
        ACXUserStayStartDateKey           : ((self.startDate != nil) ? @(self.startDate.timeIntervalSince1970) : [NSNull null]),
        ACXUserStayEndDateKey             : ((self.endDate != nil) ? @(self.endDate.timeIntervalSince1970) : [NSNull null]),
        ACXUserStaySelectedPlaceKey       : (self.selectedPlace.JSONDictionary ?: [NSNull null]),
        ACXUserStayTopCandidatePlacesKey  : ^NSArray * {
            NSMutableArray *topCandidatePlaces = [NSMutableArray array];
            [self.topCandidatePlaces enumerateObjectsUsingBlock:^(ACXPlace *place, NSUInteger idx, BOOL *stop) {
                [topCandidatePlaces addObject:place.JSONDictionary];
            }];
            return [topCandidatePlaces copy];
        }()
    };
}

@end
