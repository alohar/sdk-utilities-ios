//
//  ACXPlace+JSON.m
//  Alohar Mobile Sample
//
//  Created by Steven Byrd on 9/22/15.
//
//

#import "ACXJsonUtils.h"
#import "ACXPlace+JSON.h"


NSString *const ACXPlaceIdentifierKey   = @"identifier";
NSString *const ACXPlaceNameKey         = @"name";
NSString *const ACXPlaceCoordinateKey   = @"coordinate";
NSString *const ACXPlaceAddressKey      = @"address";
NSString *const ACXPlaceCategoriesKey   = @"categories";


#pragma mark -
@implementation ACXPlace (JSON)

- (NSDictionary *)JSONDictionary {
    return @{
        ACXPlaceIdentifierKey : (self.identifier ?: [NSNull null]),
        ACXPlaceNameKey       : (self.name ?: [NSNull null]),
        ACXPlaceCoordinateKey : [ACXJsonUtils JSONDictionaryForCoordinate:self.coordinate],
        ACXPlaceAddressKey    : (self.address ?: [NSNull null]),
        ACXPlaceCategoriesKey : (self.categories ?: [NSNull null])
    };
}

@end
