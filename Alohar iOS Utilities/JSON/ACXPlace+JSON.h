//
//  ACXPlace+JSON.h
//  Alohar Mobile Sample
//
//  Created by Steven Byrd on 9/22/15.
//
//

@import Foundation;

#import <Alohar/ACXPlace.h>


extern NSString *const ACXPlaceIdentifierKey;
extern NSString *const ACXPlaceNameKey;
extern NSString *const ACXPlaceCoordinateKey;
extern NSString *const ACXPlaceAddressKey;
extern NSString *const ACXPlaceCategoriesKey;


#pragma mark -
@interface ACXPlace (JSON)

- (NSDictionary *)JSONDictionary;

@end
