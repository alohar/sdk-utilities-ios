//
//  ACXJsonUtils.m
//  Alohar Mobile Sample
//
//  Created by Steven Byrd on 9/17/15.
//
//

#import "ACXJsonUtils.h"
#import "ACXUserStay+JSON.h"


NSString *const ACXLatitudeKey  = @"latitude";
NSString *const ACXLongitudeKey = @"longitude";


#pragma mark -
@implementation ACXJsonUtils

+ (void)runJsonTest {
    NSString *dataFile = [[NSBundle mainBundle] pathForResource:@"ACXUserStayListExample" ofType:@"dat"];
    NSArray *userStayList = [NSKeyedUnarchiver unarchiveObjectWithFile:dataFile];
    [userStayList enumerateObjectsUsingBlock:^(ACXUserStay *userStay, NSUInteger idx, BOOL *stop) {
        NSLog(@"%@", userStay.JSONDictionary);
    }];
}

+ (NSDictionary *)JSONDictionaryForCoordinate:(CLLocationCoordinate2D)coordinate {
    return @{
        ACXLatitudeKey  : @(coordinate.latitude),
        ACXLongitudeKey : @(coordinate.longitude)
    };
}

@end
