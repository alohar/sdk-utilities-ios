//
//  ACXJsonUtils.h
//  Alohar Mobile Sample
//
//  Created by Steven Byrd on 9/17/15.
//
//

@import Foundation;
@import CoreLocation;


extern NSString *const ACXLatitudeKey;
extern NSString *const ACXLongitudeKey;


#pragma mark -
@interface ACXJsonUtils : NSObject

+ (void)runJsonTest;

+ (NSDictionary *)JSONDictionaryForCoordinate:(CLLocationCoordinate2D)coordinate;

@end
