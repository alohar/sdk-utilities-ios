# sdk-utilities-ios

[![CI Status](http://img.shields.io/travis/Steven Byrd/sdk-utilities-ios.svg?style=flat)](https://travis-ci.org/Steven Byrd/sdk-utilities-ios)
[![Version](https://img.shields.io/cocoapods/v/sdk-utilities-ios.svg?style=flat)](https://bitbucket.org/alohar/sdk-utilities-ios)
[![License](https://img.shields.io/cocoapods/l/sdk-utilities-ios.svg?style=flat)](https://bitbucket.org/alohar/sdk-utilities-ios)
[![Platform](https://img.shields.io/cocoapods/p/sdk-utilities-ios.svg?style=flat)](https://bitbucket.org/alohar/sdk-utilities-ios)

## Usage

Add the cocoapod to your project's podfile and import the appropriate categories.

## Requirements

iOS 7+

Your project must be linked against the Alohar framework.

## Installation

sdk-utilities-ios is available through [Alohar](http://developer.alohar.com).

If you are using Cocoapods to manage your project dependencies, add the following line to your Podfile:

```ruby
pod 'sdk-utilities-ios', :git => 'https://bitbucket.org/alohar/sdk-utilities-ios.git', :commit => '<LATEST_COMMIT_HASH>'
```

NOTE: Be sure to replace <LATEST_COMMIT_HASH> with the hash of the appropriate commit in the sdk-utilities-ios repository.

Once the cocoapod has been installed, you must find the "sdk-utilities-ios" build target of the "Pods" project and update its "Framework search paths" build setting to include the path to Alohar.framework (see "Build Settings Example" for a screenshot).

## Author

Alohar Mobile, support@alohar.com

## License

sdk-utilities-ios is available under the MIT license. See the LICENSE file for more info.
